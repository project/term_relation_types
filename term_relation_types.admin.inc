<?php


function term_relation_types_overview_form($form_state) {
  $form = array();
  
  // Check for confirmation forms.
  if (isset($form_state['confirm_delete'])) {
    return array_merge($form, term_relation_types_confirm_delete($form_state));
  }
  
  $types = array();
  foreach (term_relation_types_get_types() as $rtid => $type) {
    $types[$rtid] = '';
    $form['name'][$rtid] = array('#value' => l($type->name, 'admin/content/term_relation_type/add/'. $rtid));
    $form['namespace'][$rtid] = array('#value' => l($type->namespace, 'admin/content/term_relation_type/namespace_add/'. $type->namespace));
  }
  
  $form['types'] = array('#type' => 'checkboxes', '#options' => $types);
  
  $form['delete'] = array(
    '#type' => 'submit', 
    '#value' => t('Delete'),
  );
  
  $form['#theme'] = 'term_relation_types_overview_form';
  
  return $form;
}

/**
 * Form builder for the term delete form.
 */
function term_relation_types_confirm_delete(&$form_state) {
  $form = array();
  $form['#tree'] = TRUE;
  foreach ($form_state['values']['types'] as $rtid => $checked) {
    if ($checked) {
      $form['types'][$rtid] = array(
        '#type' => 'value',
        '#value' => $rtid,
      );
    }
  }
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the selected relation types'),
                  'admin/content/term_relation_type',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}


function term_relation_types_overview_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    if ($form_state['values']['delete'] === TRUE) {

      foreach ($form_state['values']['types'] as $rtid => $checked) {
        term_relation_types_delete_type($rtid);
      }
      
      drupal_set_message("Successfully deleted");
      $form_state['redirect'] = 'admin/content/term_relation_type';
      return;
    }
    // Rebuild the form to confirm the deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }
  

}

function term_relation_types_add_form($form_state, $rtid = NULL) {
  $form = array();
  
  if (isset($rtid)) {
    $rel_type = term_relation_types_get_type($rtid);
    $form['rtid'] = array('#type' => 'value', '#value' => $rtid);
  }
  
  $form['name'] = array(
    '#type' => 'textfield', 
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => isset($rel_type) ? $rel_type->name : '',
    '#maxlength' => 255, 
  );
  
  $namespaces[0] = t('<None>');
  foreach (term_relation_types_get_namespaces() as $namespace) {
    $namespaces[$namespace->namespace] = $namespace->namespace;
  }
  
  $form['namespace'] = array(
    '#type' => 'select', 
    '#title' => t('Namespace (optional)'),
    '#options' => $namespaces, 
    '#default_value' => (isset($rel_type) && isset($rel_type->namespace)) ? $rel_type->namespace : 0,
  );
  
  $form['description'] = array( 
    '#type' => 'textarea', 
    '#title' => t('Description'),
    '#default_value' => isset($rel_type) ? $rel_type->description : '',
  );
  
  $form['save'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );
  
  return $form;
}

function term_relation_types_add_form_validate($form, &$form_state) {
  $existing_types = term_relation_types_get_types();
  $name = $form_state['values']['name'];
  $namespace = ($form_state['values']['namespace']) ? $form_state['values']['namespace'] : NULL;
  foreach ($existing_types as $rtid => $type) {
    if ($rtid != $form_state['values']['rtid']) { 
      if ($type->name == $name && $type->namespace == $namespace) {
        form_set_error('', t('Relation Type allready existing!'));
      }
    }
  }
}

function term_relation_types_add_form_submit($form, &$form_state) {
  $namespace = ($form_state['values']['namespace']) ? $form_state['values']['namespace'] : NULL;
  term_relation_types_save_type($form_state['values']['name'], $namespace, $form_state['values']['description'], $form_state['values']['rtid']);
  drupal_set_message("The relation type ". $form_state['values']['name'] ." has been saved");
 
}

function term_relation_types_namespace_overview_form($form_state) {
  $form = array();
  
  // Check for confirmation forms.
  if (isset($form_state['confirm_delete'])) {
    return array_merge($form, term_relation_types_namespaces_confirm_delete($form_state));
  }
  
  foreach (term_relation_types_get_namespaces() as $namespace) {
    $namespaces[$namespace->namespace] = '';
    $form['name'][$namespace->namespace] = array('#value' => l($namespace->namespace, 'admin/content/term_relation_type/namespace_add/'. $namespace->namespace));
    $form['uri'][$namespace->namespace] = array('#value' => check_plain($namespace->uri));
  }
  
  $form['namespaces'] = array('#type' => 'checkboxes', '#options' => $namespaces);
  
  $form['delete'] = array(
    '#type' => 'submit', 
    '#value' => t('Delete'),
  );
  
  $form['#theme'] = 'term_relation_types_namespace_overview_form';
  
  return $form;
}

/**
 * Form builder for the term delete form.
 */
function term_relation_types_namespaces_confirm_delete(&$form_state) {
  $form = array();
  $form['#tree'] = TRUE;
  foreach ($form_state['values']['namespaces'] as $name => $checked) {
    if ($checked) {
      $form['namespaces'][$name] = array(
        '#type' => 'value',
        '#value' => $name,
      );
    }
  }
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  return confirm_form($form,
                  t('Are you sure you want to delete the selected namespaces'),
                  'admin/content/term_relation_type/namespace_list',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

function term_relation_types_namespace_overview_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    if ($form_state['values']['delete'] === TRUE) {

      foreach ($form_state['values']['namespaces'] as $namespace => $checked) {
        term_relation_types_delete_namespace($namespace);
      }
      
      drupal_set_message("Successfully deleted");
      $form_state['redirect'] = 'admin/content/term_relation_type/namespace_list';
      return;
    }
    // Rebuild the form to confirm the deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }
}

function term_relation_types_namespace_add_form($form_state, $namespace = NULL) {
  $form = array();
  
  if (isset($namespace)) {
    $namespace = term_relation_types_get_namespaces($namespace);
  }
  
  $form['name'] = array(
    '#type' => 'textfield', 
    '#title' => t('Name'),
    '#required' => TRUE,
    '#default_value' => isset($namespace) ? $namespace->namespace : '',
    '#maxlength' => 255, 
  );
  
  $form['uri'] = array(
    '#type' => 'textfield', 
    '#title' => t('URI'),
    '#required' => TRUE,
    '#default_value' => isset($namespace) ? $namespace->uri : '',
    '#maxlength' => 255, 
  );
  
  $form['save'] = array(
    '#type' => 'submit', 
    '#value' => t('Save'),
  );
  
  return $form;
}

function term_relation_types_namespace_add_form_validate($form, &$form_state) {
  $existing_namespaces = term_relation_types_get_namespaces();
  $name = $form_state['values']['name'];
  $uri = $form_state['values']['uri'];
  foreach ($existing_namespaces as $n => $namespace) {
    if ($namespace->namespace == $name) {
      form_set_error('', t('Namespace shortcut allready existing!'));
    }
    if ($namespace->uri == $uri) {
      form_set_error('', t('URI allready existing!'));
    }
  }
}

function term_relation_types_namespace_add_form_submit($form, &$form_state) {
  $name = $form_state['values']['name'];
  $uri = $form_state['values']['uri'];
  term_relation_types_save_namespace($name, $uri);
  drupal_set_message("Namespace saved");
}


function theme_term_relation_types_overview_form($form) {
  $header = array('', t('Name'), t('Namespace'));
  $output = '';

  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['types'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['namespace'][$key]);
      $rows[] = $row;
    }
  } 
  else {
    $rows[] = array(array('data' => t('No relation types available.'), 'colspan' => '3'));
  }

  $output .= theme('table', $header, $rows);

  $output .= drupal_render($form);

  return $output;
}

function theme_term_relation_types_namespace_overview_form($form) {
  $header = array('', t('Name'), t('URI'));
  $output = '';

  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['namespaces'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['uri'][$key]);
      $rows[] = $row;
    }
  } 
  else {
    $rows[] = array(array('data' => t('No namespaces available.'), 'colspan' => '3'));
  }

  $output .= theme('table', $header, $rows);

  $output .= drupal_render($form);

  return $output;
}
